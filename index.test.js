import { tester } from 'graphql-tester';
import faker from 'faker';

const makeRequest = tester({
  url: 'http://localhost:3131/graphql',
  contentType: 'application/json'
});

let id = '';

test('Create a user', async () => {
  const query = `
    mutation {
      signup(
        email: "${faker.internet.email()}"
        name: "Ouro"
        password: "12345678"
        passwordConfirmation: "12345678"
      ) {
        id
      }
    }`

  const response = await makeRequest(
    JSON.stringify({
      query
    })
  );

  id = response.data.signup.id;

  expect(response.data.signup.id).toBeDefined();
});

test('Create a hackathon', async () => {
  console.log(id);
  const response = await makeRequest(
    JSON.stringify({
      query: `
        mutation {
          newHackathon(
            name: "HackUSP"
            owner_id: "${id}"
          ) {
            id
          }
        }
      `
    })
  );

  expect(response.data.newHackathon.id).toBeDefined();
});
